defmodule Asteroids do
  use Application
  alias Asteroids.Window

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Define workers and child supervisors to be supervised
      # worker(Asteroids.Worker, [arg1, arg2, arg3]),
      worker(Window, [{500, 500}, "Asteroids", Asteroids.Controller]),
      worker(Asteroids.Controller, []),
      worker(GameLoop, []),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Asteroids.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
