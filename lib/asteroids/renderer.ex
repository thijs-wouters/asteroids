defmodule Asteroids.Renderer do
  def update(world, _) when is_list(world) do
    Asteroids.Window.draw(world)
  end
end

defprotocol Drawable do
  def draw(entity)
end
