defmodule Asteroids.Window do
  use GenServer

  def start_link(size, title, controller) do
    GenServer.start_link(__MODULE__, {size, title, controller}, name: __MODULE__)
  end

  def init({size, title, controller}) do
    wx = :wx.new
    frame = :wxFrame.new(wx, -1, String.to_char_list(title))
    gl = :wxGLCanvas.new(frame, size: size, attribList: [:wx_const.wx_gl_rgba, :wx_const.wx_gl_doublebuffer, 0])

    :wxWindow.setMinSize(gl, size)
    :wxWindow.setMaxSize(gl, size)
    :wxWindow.setSize(gl, size)

    mainSizer = :wxBoxSizer.new(:wx_const.wx_vertical)
    :wxSizer.setMinSize(mainSizer, size)
    :wxSizer.fit(mainSizer, frame)

    :wxWindow.setMinSize(frame, :wxWindow.getSize(frame))
    :wxWindow.setMaxSize(frame, :wxWindow.getSize(frame))
    :wxWindow.setSize(frame, :wxWindow.getSize(frame))

    :wxWindow.show(frame)
    :wxWindow.setFocus(frame)

    :wxGLCanvas.setCurrent(gl)

    {width, height} = :wxWindow.getClientSize(gl)

    :gl.viewport(0, 0, width, height)
    :gl.matrixMode(:gl_const.gl_projection)
    :gl.loadIdentity()
    :gl.ortho(0, width, height, 0, -1, 1)

    :gl.matrixMode(:gl_const.gl_modelview)
    :gl.loadIdentity()
    :gl.clearColor(0.0, 0.0, 0.0, 1.0)

    :gl.enable(:gl_const.gl_blend)
    :gl.enable(:gl_const.gl_texture_2d)
    :gl.blendFunc(:gl_const.gl_src_alpha, :gl_const.gl_one_minus_src_alpha)
    :gl.shadeModel(:gl_const.gl_flat)

    :wxWindow.connect(frame, :key_up, callback: &key_event/2)
    :wxWindow.connect(frame, :char, callback: &key_event/2)
    :wxWindow.connect(frame, :key_down, callback: &key_event/2)
    :wxWindow.connect(gl, :key_up, callback: &key_event/2)
    :wxWindow.connect(gl, :char, callback: &key_event/2)
    :wxWindow.connect(gl, :key_down, callback: &key_event/2)

    :wxWindow.raise(gl)
    :wxWindow.setFocus(gl)

    {:ok, %{frame: frame, gl: gl, controller: controller}}
  end

  def draw(world) do
    GenServer.call(__MODULE__, {:draw, world})
  end

  def key_event({:wx, _, _, _, {:wxKey, event, _, _, key_code, _, _, _, _, _, _, _, _}}, _) do
    GenServer.cast(__MODULE__, {event, key_code})
  end

  def handle_call({:draw, world}, _from, state = %{gl: gl}) do
    :gl.clear(:gl_const.gl_color_buffer_bit)
    Enum.each(world, &Drawable.draw/1)
    :wxGLCanvas.swapBuffers(gl)
    {:reply, world, state}
  end

  def handle_cast({:key_up, key_code}, state = %{controller: controller}) do
    controller.key(:up, key_code)
    {:noreply, state}
  end

  def handle_cast({:key_down, key_code}, state = %{controller: controller}) do
    controller.key(:down, key_code)
    {:noreply, state}
  end

  def handle_cast({:register, controller}, state) do
    {:noreply, %{state | controller: controller}}
  end
end
