defmodule GameLoop do
  alias Asteroids.Renderer
  alias Asteroids.Controller

  def start_link do
    loop [
      Asteroids.SpaceShip.new,
    ], :erlang.monotonic_time
  end

  def loop(world, previous_time) do
    current_time = :erlang.monotonic_time
    diff = current_time - previous_time
    :timer.sleep(16)
    world = Controller.update(world, diff)
    world = Update.update(world, diff)
    world = Renderer.update(world, diff)
    loop(world, current_time)
  end
end
