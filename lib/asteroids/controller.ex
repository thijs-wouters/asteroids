defmodule Asteroids.Controller do
  def start_link do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def key(:down, key_code) do
    IO.puts "No action: #{key_code}"
  end

  def key(:up, key_code) do
    IO.puts "No action: #{key_code}"
  end

  def update(world, _time_elapsed) do
    world
  end
end
