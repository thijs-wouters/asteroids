defmodule Color do
  defstruct [:red, :green, :blue]
end
