defmodule Update do
  def start_link do
  end

  def update(world, elapsed) when is_list(world) do
    world
  end

  def update(entity = %{position: position}, elapsed) do
    %{entity | position: update(position, elapsed)}
  end
  def update(%Position{x: x, y: y}, elapsed) do
    seconds = elapsed/1000000000
    %Position{x: x + 10 * seconds, y: y + 10 * seconds}
  end
end
