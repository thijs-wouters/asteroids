defmodule Asteroids.SpaceShip do
  defstruct [
    position: %Position{
      x: 200,
      y: 200,
    },
  ]

  def new do
    %Asteroids.SpaceShip{}
  end
end

defimpl Drawable, for: Asteroids.SpaceShip do
  def draw(%Asteroids.SpaceShip{position: %Position{x: x, y: y}}) do
    :gl.color3f(1, 1, 1)
    :gl.begin(:gl_const.gl_triangle_fan)
    :gl.vertex2f(x, y)
    :gl.vertex2f(x + 10, y + 10)
    :gl.vertex2f(x, y - 20)
    :gl.vertex2f(x - 10, y + 10)
    :gl.end
  end
end
